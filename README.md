# Wrapifier

An easy-to-use modern library to wrap whatever you want !

[![pipeline status](https://gitlab.com/lukasz.swider.p/Wrapifier/badges/master/pipeline.svg)](https://gitlab.com/lukasz.swider.p/Wrapifier/commits/master) [![coverage report](https://gitlab.com/lukasz.swider.p/Wrapifier/badges/master/coverage.svg)](https://gitlab.com/lukasz.swider.p/Wrapifier/commits/master)

## Getting Started

See [Wiki](https://gitlab.com/lukasz.swider.p/Wrapifier/wikis/home)

## Built With

* [TypeScript](http://www.typescriptlang.org) - TypeScript is a superset of JavaScript that compiles to clean JavaScript output.

## Authors

* **Lukasz Swider** - *Initial work*

See also the list of [contributors](https://gitlab.com/lukasz.swider.p/Wrapifier/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
