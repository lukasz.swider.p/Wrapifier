export type DataKeys<T> = {
  [K in keyof T]: T[K] extends Function ? never : K
}[keyof T];

export type Data<T> = Pick<T, DataKeys<T>>;

/**
 * Element Options
 *
 * @export
 * @interface WWElement
 */
export interface WWElement {
  /**
   * Element Tag Name
   *
   * @type {string}
   * @memberof WWElement
   */
  readonly tagName: string;

  /**
   * Element class
   *
   * @type {string}
   * @memberof WWElement
   */
  readonly class: string;
}

/**
 * Wrapifier, An easy-to-use modern library to wrap whatever you want !
 *
 * @export
 * @class Wrapifier
 * @template T The template.
 */
export default class Wrapifier<T extends Wrapifier<T>> {
  /**
   * The query selector used to determine the list of elements.
   *
   * @type {string}
   * @memberof Wrapifier
   */
  public readonly qSelector: string = "[data-wrapifier]";

  /**
   * The dataset used to determine what should be wrapped.
   *
   * @type {string}
   * @memberof Wrapifier
   */
  public readonly dataset: string = "data-wrapifier";

  /**
   * The wrapper options.
   *
   * @type {WWElement}
   * @memberof Wrapifier
   */
  public readonly WElements: WWElement = {
    tagName: "div",
    class: "el"
  };

  /**
   * The wrapper options.
   *
   * @type {WWElement}
   * @memberof Wrapifier
   */
  public readonly WChars: WWElement = {
    tagName: "span",
    class: "char"
  };

  /**
   * The wrapper options.
   *
   * @type {WWElement}
   * @memberof Wrapifier
   */
  public readonly WWords: WWElement = {
    tagName: "span",
    class: "word"
  };
  /**
   * The wrapper options.
   *
   * @type {WWElement}
   * @memberof Wrapifier
   */
  public readonly WLines: WWElement = {
    tagName: "span",
    class: "line"
  };

  /**
   * Creates an instance of Wrapifier.
   * @param {Partial<Data<T>>} [data] The data.
   * @memberof Wrapifier
   */
  constructor(data?: Partial<Data<T>>) {
    if (data) Object.assign(this, data);
    this.start(document.querySelectorAll(this.qSelector));
  }

  /**
   * Start detection.
   *
   * @private
   * @param {NodeListOf<HTMLElement>} elements
   * @memberof Wrapifier
   */
  private start(elements: NodeListOf<HTMLElement>) {
    for (const element of elements) {
      const dataset = element.getAttribute(this.dataset);
      const cases: { [key: string]: () => void } = {
        me: () => this.wrapEl(element),
        chars: () => this.wrapChars(element),
        words: () => this.wrapWords(element),
        lines: () => this.wrapLines(element)
      };
      if (dataset) cases[dataset]();
    }
  }

  /**
   * Wrap an element.
   *
   * @private
   * @param {HTMLElement} element The element to be wrapped.
   * @param {string} tagName The element tag.
   * @param {string} className The class of the wrapper.
   * @memberof Wrapifier
   */
  private wrapElement(
    element: HTMLElement,
    tagName: string,
    className: string
  ) {
    if (element) {
      let wrapper: HTMLElement = document.createElement(tagName);
      wrapper.className = className;
      element.parentNode!.insertBefore(wrapper, element);
      wrapper.appendChild(element);
    }
  }

  /**
   * Wrap a string.
   *
   * @private
   * @param {HTMLElement} element The element to be wrapped.
   * @param {string} separator The character or regular expression to use to split.
   * @param {string} tagName The element tag.
   * @param {string} className The class of the wrapper.
   * @memberof Wrapifier
   */
  private wrapString(
    element: HTMLElement,
    separator: string,
    tagName: string,
    className: string
  ) {
    if (element.textContent) {
      let fragment: DocumentFragment = document.createDocumentFragment();
      let splittedTextContent = element.textContent.split(separator);
      splittedTextContent.forEach((s: string, i: number) => {
        let wrapper: HTMLElement = document.createElement(tagName);
        let t: Node = document.createTextNode(s);
        wrapper.className = className + i;
        wrapper.appendChild(t);
        fragment.appendChild(wrapper);
      });
      element.textContent = null;
      element.appendChild(fragment);
    }
  }

  /**
   * Wrap an element.
   *
   * @public
   * @param {HTMLElement} element The element to be wrapped.
   * @memberof Wrapifier
   */
  public wrapEl(element: HTMLElement) {
    this.wrapElement(element, this.WElements.tagName, this.WElements.class);
  }

  /**
   * Wrap by Characters.
   *
   * @public
   * @param {HTMLElement} element The element to be wrapped.
   * @memberof Wrapifier
   */
  public wrapChars(element: HTMLElement) {
    this.wrapString(element, "", this.WChars.tagName, this.WChars.class);
  }

  /**
   * Wrap by Words.
   *
   * @public
   * @param {HTMLElement} element The element to be wrapped.
   * @memberof Wrapifier
   */
  public wrapWords(element: HTMLElement) {
    this.wrapString(element, " ", this.WWords.tagName, this.WWords.class);
  }

  /**
   * Wrap by Lines.
   *
   * @public
   * @param {HTMLElement} element The element to be wrapped.
   * @memberof Wrapifier
   */
  public wrapLines(element: HTMLElement) {
    this.wrapString(element, "\n", this.WLines.tagName, this.WLines.class);
  }
}
