import Wrapifier from "../src/Wrapifier";

declare const jest, describe, beforeEach, it, expect;

/**
 * Create an element.
 *
 * @param {string} tagName The element tag.
 * @param {string} wrapBy The Wrapifier selector.
 * @param {string} textContent The text.
 */
function createElement(tagName: string, wrapBy: string, textContent?: string) {
  let fragment: DocumentFragment = document.createDocumentFragment();
  let element: HTMLElement = document.createElement(tagName);
  element.dataset.wrapifier = wrapBy;
  if (textContent) {
    let text: Node = document.createTextNode(textContent);
    element.appendChild(text);
  }
  fragment.appendChild(element);
  document.body.appendChild(fragment);
}

describe("Wrapifier", () => {
  beforeEach(() => {
    document.body.textContent = null;
  });

  it("should be able to wrap an Element", () => {
    createElement("div", "me");
    new Wrapifier();
    const result = document.querySelector('[data-wrapifier="me"]').parentElement
      .childElementCount;
    expect(result).toBe(1);
  });

  it("should be able to wrap an Element with user's undefined options", () => {
    createElement("div", "me");
    new Wrapifier(undefined);
    const result = document.getElementsByTagName("div");
    expect(result);
  });

  it("should be able to wrap an Element with user's null options", () => {
    createElement("div", "me");
    new Wrapifier(null);
    const result = document.getElementsByClassName("div");
    expect(result);
  });

  it("should be able to wrap an Element with user's options", () => {
    createElement("div", "me");
    new Wrapifier({
      WElements: {
        tagName: "div",
        class: "custom"
      }
    });
    const result = document.getElementsByClassName("custom");
    expect(result);
  });

  it("should be able to wrap by Chars", () => {
    createElement("p", "chars", "Character");
    new Wrapifier();
    const result = document.querySelector('[data-wrapifier="chars"]')
      .childElementCount;
    expect(result).toBe(9);
  });

  it("should be able to wrap by Words", () => {
    createElement("p", "words", "Two Words");
    new Wrapifier();
    const result = document.querySelector('[data-wrapifier="words"]')
      .childElementCount;
    expect(result).toBe(2);
  });

  it("should be able to wrap by Lines", () => {
    createElement("p", "lines", "Line\nFeed");
    new Wrapifier();
    const result = document.querySelector('[data-wrapifier="lines"]')
      .childElementCount;
    expect(result).toBe(2);
  });
});
